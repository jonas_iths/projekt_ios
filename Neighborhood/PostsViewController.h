//
//  PostsViewController.h
//  Neighborhood
//
//  Created by Jonas on 2015-04-02.
//  Copyright (c) 2015 Jonas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "NickNameViewController.h"
#import "PostTableViewCell.h"
#import <Parse/Parse.h>

@interface PostsViewController : UITableViewController <CLLocationManagerDelegate>

@property (nonatomic) NSArray *postArray;

@end
