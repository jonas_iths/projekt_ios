//
//  AppDelegate.h
//  Neighborhood
//
//  Created by Jonas on 2015-04-01.
//  Copyright (c) 2015 Jonas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic, strong) UIWindow *window;


@end

