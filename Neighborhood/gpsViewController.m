//
//  gpsViewController.m
//  Neighborhood
//
//  Created by Jonas on 2015-04-04.
//  Copyright (c) 2015 Jonas. All rights reserved.
//

#import "gpsViewController.h"

@interface gpsViewController ()
@property (weak, nonatomic) IBOutlet UILabel *longi;
@property (weak, nonatomic) IBOutlet UILabel *lati;
@property (strong, nonatomic) CLLocationManager *locationManager;




@end

@implementation gpsViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.locationManager = [[CLLocationManager alloc] init];
    
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    [self.locationManager startUpdatingLocation];
    
}
- (IBAction)gpsButton:(id)sender {
    
    NSLog(@"GPS");
    
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [self.locationManager startUpdatingLocation];
}


- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        self.longi.text = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        self.lati.text = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
    } else {
        self.longi.text = @"jonas";
    }
    
    [self.locationManager stopUpdatingLocation];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
