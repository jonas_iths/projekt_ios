//
//  PostTableViewCell.h
//  Neighborhood
//
//  Created by Jonas on 2015-04-26.
//  Copyright (c) 2015 Jonas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface PostTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *numberOfLikes;

@property (weak, nonatomic) IBOutlet UIImageView *image;

@property (weak, nonatomic) NSString *objectId;

@property (weak, nonatomic) PFObject *theParseObject;

@property (weak, nonatomic) IBOutlet UILabel *name;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textWidth;

@property (weak, nonatomic) IBOutlet UILabel *text;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageHeight;

@property (weak, nonatomic) IBOutlet UILabel *timeSinceCreated;

@property (weak, nonatomic) IBOutlet UIButton *likeButton;

@end
