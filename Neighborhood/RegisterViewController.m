//
//  RegisterViewController.m
//  Neighborhood
//
//  Created by Jonas on 2015-04-02.
//  Copyright (c) 2015 Jonas. All rights reserved.
//

#import "RegisterViewController.h"

@interface RegisterViewController ()
@property (weak, nonatomic) IBOutlet UITextField *mail;
@property (weak, nonatomic) IBOutlet UITextField *password;
@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (IBAction)cancelButton:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)registerButton:(id)sender {
    
    PFUser *user = [PFUser user];
    user.username = self.mail.text;
    user.password = self.password.text;
    user.email = self.mail.text;
    
    // other fields can be set just like with PFObject
    user[@"nickName"] = @"_";
    // NSNumber *once = 0;
    user[@"loggedInOnce"] = [NSNumber numberWithBool:NO] ;
    
    [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            NSLog(@"Registered, check mail");
        } else {
            NSString *errorString = [error userInfo][@"error"];
            // Show the errorString somewhere and let the user try again.
            NSLog(@"Error: %@", errorString);
        }
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
