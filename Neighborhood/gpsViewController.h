//
//  gpsViewController.h
//  Neighborhood
//
//  Created by Jonas on 2015-04-04.
//  Copyright (c) 2015 Jonas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface gpsViewController : UIViewController <CLLocationManagerDelegate>


@end
