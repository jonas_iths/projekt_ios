//
//  ViewController.h
//  Neighborhood
//
//  Created by Jonas on 2015-04-01.
//  Copyright (c) 2015 Jonas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <CoreLocation/CoreLocation.h>
#import "PostsViewController.h"

@interface ViewController :UIViewController <CLLocationManagerDelegate>
@end

