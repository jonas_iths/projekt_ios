//
//  PostTableViewCell.m
//  Neighborhood
//
//  Created by Jonas on 2015-04-26.
//  Copyright (c) 2015 Jonas. All rights reserved.
//

#import "PostTableViewCell.h"


@implementation PostTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (IBAction)likeButtonChange:(id)sender {
    
    
    if ([self.likeButton.currentImage isEqual:[UIImage imageNamed:@"like_red.png"]]) {
        [self.likeButton setImage:[UIImage imageNamed:@"like.png"] forState:UIControlStateNormal];
    } else {
        [self.likeButton setImage:[UIImage imageNamed:@"like_red.png"] forState:UIControlStateNormal];
    }
    
    PFQuery *query = [PFQuery queryWithClassName:@"Likes"];
    [query whereKey:@"postObjectId" equalTo: [self.theParseObject objectId] ];
    [query findObjectsInBackgroundWithBlock:^(NSArray *posts, NSError *error) {
        
    if (posts.count < 1) {
        
        // No like object, create one and add to array
        PFObject *likeObject = [PFObject objectWithClassName:@"Likes"];
        likeObject[@"nickName"] = @[[PFUser currentUser][@"nickName"]];
        likeObject[@"postObjectId"] = self.objectId;
        
        [likeObject saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (succeeded) {
                [self.likeButton setImage:[UIImage imageNamed:@"like_red.png"] forState:UIControlStateNormal];
            } else {
                // NSLog(@"error");
            }
        }];
        
    } else {
        // loop trough and check if nickname in it
        NSMutableArray *arrayLikes = posts[0][@"nickName"];
        for ( int i = 0; i < arrayLikes.count; i++ ) {
            
            if ([arrayLikes[i] isEqualToString: [PFUser currentUser][@"nickName"] ]) {
                [arrayLikes removeObjectAtIndex:i];
                
                if (arrayLikes.count < 1) {
                    [posts[0] deleteInBackground];
                } else {
                    [posts[0] setObject:arrayLikes forKey:@"nickName"];
                    [posts[0] saveInBackground];
                }
                
                [self.likeButton setImage:[UIImage imageNamed:@"like.png"] forState:UIControlStateNormal];
                
                break;
            }
            
        }
        
    }
        
    }];

}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
