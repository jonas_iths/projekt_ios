//
//  AddPostViewController.m
//  Neighborhood
//
//  Created by Jonas on 2015-04-04.
//  Copyright (c) 2015 Jonas. All rights reserved.
//

#import "AddPostViewController.h"

@interface AddPostViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageHeightConstraint;
@property (weak, nonatomic) IBOutlet UITextField *textMessage;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (nonatomic) float longi;
@property (nonatomic) float lati;

@property (nonatomic) UIImagePickerController *picker;

@property (weak, nonatomic) IBOutlet UIImageView *imageLoad;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingIcon;

@property (weak, nonatomic) IBOutlet UILabel *loadingText;


@end

@implementation AddPostViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.hidden = NO;
    [self.navigationController setToolbarHidden:YES];
    
    self.textMessage.delegate = self;
    self.imageHeight.constant = self.view.frame.size.width;
    
    self.locationManager = [[CLLocationManager alloc] init];

    self.picker = [[UIImagePickerController alloc] init];
    self.picker.delegate = self;
    self.picker.allowsEditing = NO;
    self.picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
};

- (IBAction)cancelButton:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}
- (IBAction)addPhotoButton:(id)sender {
    
    [self saveImage];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)AddPostButton:(id)sender {
    
    [self presentViewController:self.picker animated:YES completion:NULL];
    
}

- (UIImage*)imageWithImage:(UIImage*)image
{
    
    // float newWidth = width/300;
    float aspectRatio = image.size.width/image.size.height;
    float newHeight = 300/aspectRatio;
    
    CGSize newSize = CGSizeMake(300, newHeight);
    
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    // UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    
    UIImage *aNewImage = [self imageWithImage:chosenImage];
    
    float aspect = aNewImage.size.width / aNewImage.size.height;
    float newHeightConstantSize = self.view.frame.size.width / aspect ;
    
    // Is image portrait?
    if ( (aNewImage.size.width/aNewImage.size.height) > 1 ) {
        // landscape
        NSLog(@"landscape");
        self.imageHeightConstraint.constant = newHeightConstantSize;
    } else {
        // portrait
        NSLog(@"portrait");
        self.imageHeightConstraint.constant = newHeightConstantSize * 0.7;
    }
    
    // NSLog(@"aspect: %f", aspect);
    // NSLog(@"bredd: %f, höjd: %f", self.view.frame.size.width, aNewImage.size.height);
    
    self.image.image = aNewImage;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

-(void)saveImage {
    
    
    if (self.textMessage.text && self.textMessage.text.length > 0 && ![self.image.image isEqual:[UIImage imageNamed:@"no_image.png"]] ) {
        
        [self showLoader];
        
        self.locationManager.delegate = self;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        [self.locationManager startUpdatingLocation];                        
        
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Couldn´t add post"
                                                        message:@"You must enter a text message and take a picture before you can add a post."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
    }
    
}

- (void)showLoader {
    
    // show load stuff
    [self.view bringSubviewToFront:self.imageLoad];
    [self.view bringSubviewToFront:self.loadingIcon];
    [self.loadingIcon startAnimating];
    [self.view bringSubviewToFront:self.loadingText];
    
}

- (void)hideLoader {
    
    // show load stuff
    [self.view sendSubviewToBack:self.imageLoad];
    [self.view sendSubviewToBack:self.loadingIcon];
    [self.loadingIcon stopAnimating];
    [self.view sendSubviewToBack:self.loadingText];
    
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    
    [self hideLoader];
    
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        
        self.longi = currentLocation.coordinate.longitude;
        self.lati = currentLocation.coordinate.latitude;
        NSLog(@"lati: %f", self.lati);
        
           } else {
        NSLog(@"GPS error");
        [self hideLoader];
    }
    
    [self.locationManager stopUpdatingLocation];
    
    UIImage *imageToSave = [self imageWithImage:self.image.image];
    NSData *imageData = UIImageJPEGRepresentation(imageToSave, 55);
    PFFile *imageFile = [PFFile fileWithName:@"postImage.jpg" data:imageData];
    [imageFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            if (succeeded) {
                
                PFGeoPoint *point = [PFGeoPoint geoPointWithLatitude:self.lati longitude:self.longi];
                
                NSNumber *nsHeight = [NSNumber numberWithUnsignedInt:imageToSave.size.height];
                
                NSTimeInterval timeInMiliseconds = [[NSDate date] timeIntervalSince1970];
                NSNumber *nsMilli = [NSNumber numberWithUnsignedInt:timeInMiliseconds];
                
                PFObject *postObject = [PFObject objectWithClassName:@"Posts"];
                postObject[@"image"] = imageFile;
                postObject[@"text"] = self.textMessage.text;
                postObject[@"authorName"] = [PFUser currentUser][@"nickName"];
                postObject[@"geoP"] = point;
                postObject[@"height"] = nsHeight;
                postObject[@"milliseconds"] = nsMilli;
                
                [postObject saveInBackground];
                [self hideLoader];
                [self dismissViewControllerAnimated:YES completion:nil];
                
            }
        } else {
            // Handle error
            [self hideLoader];
        }
    }];
    
    
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
