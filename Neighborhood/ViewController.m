//
//  ViewController.m
//  Neighborhood
//
//  Created by Jonas on 2015-04-01.
//  Copyright (c) 2015 Jonas. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextField *mail;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (nonatomic) CLLocationManager *locationManager;
@property (nonatomic) NSString *longitude;
@property (nonatomic) NSString *latitude;
@property (nonatomic) NSArray *nearbyUsers;

@property (weak, nonatomic) IBOutlet UIImageView *loadingBlackBack;
@property (weak, nonatomic) IBOutlet UILabel *loadingText;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loader;

@property (weak, nonatomic) IBOutlet UIImageView *logoFrame;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoConstant;
@property (weak, nonatomic) IBOutlet UIImageView *logo;
@end

@implementation ViewController


- (IBAction)forgotButton:(id)sender {

    [PFUser requestPasswordResetForEmailInBackground:@"jonas@jonasekstrom.se"];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Check you mail"
                                                    message:@"You can now reset your password. Check your email to proceed."
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
        
}

- (void)showLoader {
    
    // show load stuff
    [self.view bringSubviewToFront:self.loadingBlackBack];
    [self.view bringSubviewToFront:self.loader];
    [self.loader startAnimating];
    [self.view bringSubviewToFront:self.loadingText];
    
}

- (void)hideLoader {
    
    // show load stuff
    [self.view sendSubviewToBack:self.loadingBlackBack];
    [self.view sendSubviewToBack:self.loader];
    [self.loader stopAnimating];
    [self.view sendSubviewToBack:self.loadingText];
    
}

- (IBAction)loginButton:(id)sender {
    
    NSString *name = self.mail.text;
    NSString *pass = self.password.text;
    
    [self showLoader];
    
    [PFUser logInWithUsernameInBackground:name password:pass
                                    block:^(PFUser *user, NSError *error) {
                                        if (user) {
                                            
                                            // Do stuff after successful login.
                                            
                                            NSNumber *val = user[@"emailVerified"];
                                            BOOL success = [val boolValue];
                                            if ( success ) {
                                                
                                                NSNumber *val2 = user[@"loggedInOnce"];
                                                BOOL firstTime = [val2 boolValue];
                                                
                                              
                                
                                                if( firstTime ) {

                                                    [self hideLoader];
                                                    [self performSegueWithIdentifier:@"loginSegue" sender:self];
                                                    
                                                } else {
                                                
                                                    [self hideLoader];
                                                    [self performSegueWithIdentifier:@"chooseNickNameSegue" sender:self];
                                                    
                                                }                                                
                                                
                                            } else {
                                                
                                                // user needs to verify by email
                                                [self hideLoader];
                                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Couldn´t login"
                                                                                                message:@"We sent you a verification mail in which you need to click a link to confirm your registration before you can log in."
                                                                                               delegate:nil
                                                                                      cancelButtonTitle:@"OK"
                                                                                      otherButtonTitles:nil];
                                                [alert show];
                                                
                                            }
                                            
                                        } else {
                                            [self hideLoader];
                                            NSLog(@"The login failed. Check error to see why");
                                        }
                                    }];

    
}



-(void)getPosts {

//    PFGeoPoint *swOfSF = [PFGeoPoint geoPointWithLatitude:37.708813 longitude:-122.526398];
//    PFGeoPoint *neOfSF = [PFGeoPoint geoPointWithLatitude:37.822802 longitude:-122.373962];
//    PFQuery *query = [PFQuery queryWithClassName:@"Posts"];
//    [query whereKey:@"geoP" withinGeoBoxFromSouthwest:swOfSF toNortheast:neOfSF];
//  NSArray *posts = [query findObjects];
    
    // [self.locationManager startUpdatingLocation];
    
    
//    if(self.working) {
//        return;
//    }
//    
//    dispatch_async(dispatch_get_global_queue(0, 0), ^{
//       
//        self.working = YES;
//
    
    
  //  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        
       
        
//        PFGeoPoint *swOfSF = [PFGeoPoint geoPointWithLatitude:0.708813 longitude:-122.526398];
//        PFGeoPoint *neOfSF = [PFGeoPoint geoPointWithLatitude:89.822802 longitude:-122.373962];
//        PFQuery *query = [PFQuery queryWithClassName:@"Posts" ];
//        [query orderByDescending:@"createdAt"];
//        query.limit = 15;
//        [query whereKey:@"geoP" withinGeoBoxFromSouthwest:swOfSF toNortheast:neOfSF];
//        NSArray *posts = [query findObjects];
//        NSLog(@"COUNT: %lu", posts.count);
        
 //   });
    
    
//
    //dispatch_async(dispatch_get_main_queue(), ^() {
       
    //
//            self.data = posts;
//            [self.tableView reloadData];
//            
    // });
//        
//        self.working = NO;
//        
//    });
    
    
//    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
//        if (!error) {
//            
//            //you will get all your objectsas per u created ur object in parse.
//            NSArray *posts = [query findObjects];
//            NSLog(@"COUNT: %lu", (unsigned long)posts.count);
//            
//        }
//    }];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.hidden = NO;
    
    self.logoConstant.constant = self.logoFrame.frame.size.height / 1.5;
    
    // self.mail.delegate = self;
    // self.password.delegate = self;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
};

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

-(void)viewDidAppear:(BOOL)animated {
    self.navigationController.navigationBar.hidden = NO;
    
    [self.navigationController setToolbarHidden:YES];

}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {        
       
        [self.locationManager stopUpdatingLocation];
        
        dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
        dispatch_async(myQueue, ^{
            // Perform long running process
            PFGeoPoint *currentLocation =  [PFGeoPoint geoPointWithLocation:newLocation];
            PFQuery *locationQuery = [PFQuery queryWithClassName:@"Posts"];
            
            [locationQuery whereKey:@"geoP" nearGeoPoint:currentLocation withinKilometers:1.0];
            locationQuery.limit = 15;
            [locationQuery orderByDescending:@"createdAt"];
            self.nearbyUsers = [locationQuery findObjects];
            
            // NSLog(@"COUNT:    %lu    .....", self.nearbyUsers.count);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // Update the UI
                [self performSegueWithIdentifier:@"loginSegue" sender:self];
                
            });
        });
        
        NSLog(@"stopped gps");
        [self.locationManager stopUpdatingLocation];
        
    }
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if ([[segue identifier] isEqualToString:@"loginSegue"])
    {
        // Get reference to the destination view controller
        // PostsViewController *vc = [segue destinationViewController];
        
        // Pass any objects to the view controller here, like...
        // vc.postArray = self.nearbyUsers;
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
