//
//  TableViewCell.m
//  Neighborhood
//
//  Created by Jonas on 2015-04-26.
//  Copyright (c) 2015 Jonas. All rights reserved.
//

#import "TableViewCell.h"

@implementation TableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
