//
//  NickNameViewController.m
//  Neighborhood
//
//  Created by Jonas on 2015-04-13.
//  Copyright (c) 2015 Jonas. All rights reserved.
//

#import "NickNameViewController.h"
#import "PostsViewController.h"

@interface NickNameViewController ()

@property (weak, nonatomic) IBOutlet UITextField *nickName;

@end

@implementation NickNameViewController

- (IBAction)nickButton:(id)sender {
    
    // should not be empty, not too long, not, not too short?
    
    PFQuery *query = [PFQuery queryWithClassName:@"_User"];
    [query whereKey:@"nickName" equalTo:self.nickName.text ];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            // The find succeeded.
            
            if (objects.count > 0) {
            
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ooops! Nickname taken!"
                                                                message:@"Please try another one"
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            } else {
            
                PFUser *currentUser = [PFUser currentUser];
                if (currentUser) {
                    currentUser[@"nickName"] = self.nickName.text;
                    currentUser[@"loggedInOnce"] = [NSNumber numberWithBool:YES];
                    [currentUser saveInBackground];
                    // segue to postsViewController
                    
                    [self performSegueWithIdentifier:@"toPostFromNickSegue" sender:self];
                    // [self dismissViewControllerAnimated:YES completion:nil];
                    
                } else {
                    NSLog(@"errror");
                }
                
            }
            
            NSLog(@"Successfully retrieved %lu.", (unsigned long)objects.count);
            // Do something with the found objects
            for (PFObject *object in objects) {
                NSLog(@"%@", object.objectId);
            }
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    }];
    
    // check if username is unique
    if (YES) {
    
    
    }
    
    
}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

//// In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//
//    UINavigationController *navController = segue.destinationViewController;
//    
//    PostsViewController *postViewController = navController.viewControllers[0];
//    assert(postViewController);
//    postViewController.nickNameViewController = self;
//    
//}

@end
