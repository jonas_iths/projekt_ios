//
//  PostsViewController.m
//  Neighborhood
//
//  Created by Jonas on 2015-04-02.
//  Copyright (c) 2015 Jonas. All rights reserved.
//

#import "PostsViewController.h"

@interface PostsViewController ()
@property (nonatomic) CLLocationManager *locationManager;
@property (nonatomic) NSString *longitude;
@property (nonatomic) NSString *latitude;
@property (strong, nonatomic) PostTableViewCell *customCell;

@end

@implementation PostsViewController

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
                
        [self.locationManager stopUpdatingLocation];
        
        dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
        dispatch_async(myQueue, ^{
            // Perform long running process
            PFGeoPoint *currentLocation =  [PFGeoPoint geoPointWithLocation:newLocation];
            PFQuery *locationQuery = [PFQuery queryWithClassName:@"Posts"];
            
            [locationQuery whereKey:@"geoP" nearGeoPoint:currentLocation withinKilometers:1.0];
            locationQuery.limit = 15;
            [locationQuery orderByDescending:@"createdAt"];
            self.postArray = [locationQuery findObjects];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // Update the UI
                
                 NSLog(@"reloading tableview");
                [self.tableView setContentOffset:CGPointZero animated:YES];
                [self.tableView reloadData];
                
                
            });
        });
        
        NSLog(@"stopped gps");
        [self.locationManager stopUpdatingLocation];
        
    }
}


- (IBAction)logoutButton:(id)sender {

//    assert(self.nickNameViewController);
//    [self.nickNameViewController dismissViewControllerAnimated:NO completion:nil];
    
    //[self dismissViewControllerAnimated:YES completion:nil];
    
    [self.navigationController setToolbarHidden:YES];

    [self.navigationController popToRootViewControllerAnimated:YES];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView setSeparatorColor:[UIColor whiteColor]];
    
    [self.navigationController setToolbarHidden:NO];    
    
    self.navigationController.toolbar.barTintColor = [UIColor colorWithRed:(6/255.0) green:(33/255.0) blue:(86/255.0) alpha:1];

    
    self.refreshControl = [[UIRefreshControl alloc] init];

    self.refreshControl.backgroundColor = [UIColor colorWithRed:(6/255.0) green:(33/255.0) blue:(86/255.0) alpha:1];
    
    self.refreshControl.tintColor = [UIColor whiteColor];
    [self.refreshControl addTarget:self
                            action:@selector(reloadTable)
                  forControlEvents:UIControlEventValueChanged];
    
    self.locationManager = [[CLLocationManager alloc] init];
    
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
}

- (void)reloadTable {
    
    [self.locationManager startUpdatingLocation];
    
    if (self.refreshControl) {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM d, h:mm a"];
        NSString *title = [NSString stringWithFormat:@"Pull down to refresh!"];
        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor]
                                                                    forKey:NSForegroundColorAttributeName];
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
        self.refreshControl.attributedTitle = attributedTitle;
        
        [self.refreshControl endRefreshing];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
//    if (self.postArray) {
//        
//        self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
//        return 1;
//        
//    } else {
//        
//        // Display a message when the table is empty
//        UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];        
//        
//        messageLabel.text = @"No posts made in this neighborhood so far. Please pull down to refresh or be the first to make a post here!";
//        messageLabel.textColor = [UIColor blackColor];
//        messageLabel.numberOfLines = 0;
//        messageLabel.textAlignment = NSTextAlignmentCenter;
//        messageLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:14];
//        [messageLabel sizeToFit];
//        
//        self.tableView.backgroundView = messageLabel;
//        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//        
//    }
//    
//    return 0;

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number of rows in the section.
    return self.postArray.count;
}

- (NSString*)timePast:(NSIndexPath *)indexPath {
    
    NSTimeInterval timeSeconds = [[NSDate date] timeIntervalSince1970];
    NSInteger time = timeSeconds;
    NSInteger timeCreated = [self.postArray[indexPath.row][@"milliseconds"] integerValue];
    NSInteger newTime = (time - timeCreated);
    NSString *timeUnit;
    
    if(newTime < 60) {
        // display in seconds
        timeUnit = @"s";
    } else if (newTime/60 < 60) {
        // display in minutes
        newTime = newTime/60;
        timeUnit = @"m";
    } else if (newTime/60/60 < 24) {
        // display in hours
        newTime = newTime/60/60;
        timeUnit = @"h";
    } else if (newTime/60/60/24 < 28) {
        // display in days
        newTime = newTime/60/60/24;
        timeUnit = @"d";
    } else if (newTime/60/60/24/7 < 10) {
        // display in weeks
        newTime = newTime/60/60/24;
        timeUnit = @"w";
    } else {
        // display date
        NSDate *strDate = [self.postArray[indexPath.row]createdAt];
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        
        return [NSString stringWithFormat:@"%@", [dateFormat stringFromDate:strDate]];
        
    }
    
    return [NSString stringWithFormat:@"%ld%@", (long)newTime, timeUnit];
 
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PostTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"myCell" forIndexPath:indexPath];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    // PostTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    
    // cell.text.textContainer.lineFragmentPadding = 0;
    // cell.text.textContainerInset = UIEdgeInsetsZero;
    
    cell.textWidth.constant = self.view.frame.size.width - 20;
    cell.name.text = [PFUser currentUser][@"nickName"];
    cell.objectId = [self.postArray[indexPath.row]objectId];
    cell.theParseObject = self.postArray[indexPath.row];
    
    cell.text.text = self.postArray[indexPath.row][@"text"];
    cell.timeSinceCreated.text = [self timePast:indexPath];
    cell.likeButton.tag = indexPath.row;
    
    PFQuery *query = [PFQuery queryWithClassName:@"Likes"];
    [query whereKey:@"postObjectId" equalTo: cell.objectId];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            // The find succeeded.
           
            if (objects.count > 0) {
                
                NSArray *likeArray = objects[0][@"nickName"];
                
                if (likeArray.count < 2) {
                    cell.numberOfLikes.text =
                    [NSString stringWithFormat:@"%lu like", (unsigned long)likeArray.count];

                } else {
                    cell.numberOfLikes.text =
                    [NSString stringWithFormat:@"%lu likes", (unsigned long)likeArray.count];
                }
                
            } else {
                
                cell.numberOfLikes.text = [NSString stringWithFormat:@""];
                
            }
            
        } else {
            // Log details of the failure
        }
    }];
    
    [cell.likeButton addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    PFFile *eventImage = self.postArray[indexPath.row][@"image"];
    
    if(eventImage != NULL)
    {
        
        [eventImage getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error) {
            
            UIImage *thumbnailImage = [UIImage imageWithData:imageData];

            float h = thumbnailImage.size.height;
            float w = thumbnailImage.size.width;
            
            if (h > w) {
                // portrait
             
                // cell.imageHeight.constant = (self.view.frame.size.width * (thumbnailImage.size.height / thumbnailImage.size.width)) * 0.7;
                
                cell.imageHeight.constant = (self.view.frame.size.width * (thumbnailImage.size.height / 300.0f)) * 0.7;
                
                // NSLog(@"HHHHHHHH: %f", cell.imageHeight.constant);
                
            } else {
                // landscape

                cell.imageHeight.constant = self.view.frame.size.width * (thumbnailImage.size.height / 300.0f);
                // NSLog(@"HHHHHHHH: %f", h);
            }
            
            cell.image.image = thumbnailImage;
            
        }];
        
    }
    
    if (indexPath.row == [self.postArray count] - 1)
    {
        NSLog(@"load more");
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // Calculate a height based on a cell
    if(!self.customCell) {
        
        self.customCell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
        
    }
    
    // Configure the cell
    
    self.customCell.text.text = self.postArray[indexPath.row][@"text"];
    // [self.customCell.text sizeToFit];
    
    CGSize maximumLabelSize = CGSizeMake(self.view.frame.size.width-20, 1000);
    CGSize expectedLabelSize = [self.postArray[indexPath.row][@"text"] sizeWithFont: self.customCell.text.font constrainedToSize:maximumLabelSize lineBreakMode: self.customCell.text.lineBreakMode];
    
    //adjust the label the the new height.
    CGRect newFrame =  self.customCell.text.frame;
    newFrame.size.height = expectedLabelSize.height;
    self.customCell.text.frame = newFrame;
    
    NSNumber *h = self.postArray[indexPath.row][@"height"];
    float hInt = [h floatValue];
    
    if (hInt > 300) {
        // portrait
        hInt = (self.view.frame.size.width * (hInt / 300.0f)) * 0.7;
        
    } else {
        // landscape
        hInt = (self.view.frame.size.width * (hInt / 300.0f));
        
    }
    
    // NSLog(@"HÖJDEN: %f", self.customCell.text.frame.size.height);
    
    return self.customCell.text.frame.size.height + self.customCell.name.frame.size.height + 1 + hInt + 7 + 40 + 20 + 21;
   
}
- (void)viewDidAppear:(BOOL)animated {
    NSLog(@"did appear");
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    self.locationManager.delegate = self;
    [self.locationManager startUpdatingLocation];
    
}




- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 500;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)yourButtonClicked:(UIButton*)sender
{
    
    NSLog(@"button CLICKED: %ld", (long)sender.tag);
    
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
